#-------------definition------------

CC = g++
DOT_png = dot -T png
TFILE = test
T_IN = test
T_OUT = test

SFILE1 = misex1
S_IN1 = misex1
S_OUT1 = misex1

SFILE2 = table3
S_IN2 = table3
S_OUT2 = table3

SFILE3 = vg2
S_IN3 = vg2
S_OUT3 = vg2

SFILE4 = 5xp1
S_IN4 = 5xp1
S_OUT4 = 5xp1

SFILE5 = apex1
S_IN5 = apex1
S_OUT5 = apex1

OUT_DOT = out
run = ./lsim


#--------------commands-------------

sample: compile  test  test_to_graph

compile: source.cpp
	$(CC) source.cpp -o lsim -std=c++11 -lm

test: $(TFILE)1/$(T_IN)1.blif $(TFILE)1/$(T_IN)1.sti $(TFILE)2/$(T_IN)2.blif $(TFILE)2/$(T_IN)2.sti $(TFILE)3/$(T_IN)3.blif $(TFILE)3/$(T_IN)3.sti
	$(run) $(TFILE)1/$(T_IN)1.blif $(TFILE)1/$(T_IN)1.sti $(TFILE)1/$(T_OUT)1.res
	mv $(OUT_DOT).dot $(TFILE)1/$(OUT_DOT).dot
	$(run) $(TFILE)2/$(T_IN)2.blif $(TFILE)2/$(T_OUT)2.sti $(TFILE)2/$(T_OUT)2.res
	mv $(OUT_DOT).dot $(TFILE)2/$(OUT_DOT).dot
	$(run) $(TFILE)3/$(T_IN)3.blif $(TFILE)3/$(T_OUT)3.sti $(TFILE)3/$(T_OUT)3.res
	mv $(OUT_DOT).dot $(TFILE)3/$(OUT_DOT).dot

test_to_graph:	$(TFILE)1/$(OUT_DOT).dot  $(TFILE)2/$(OUT_DOT).dot  $(TFILE)3/$(OUT_DOT).dot
	$(DOT_png) $(TFILE)1/$(OUT_DOT).dot > $(TFILE)1/$(T_OUT)1.png
	$(DOT_png) $(TFILE)2/$(OUT_DOT).dot > $(TFILE)2/$(T_OUT)2.png
	$(DOT_png) $(TFILE)3/$(OUT_DOT).dot > $(TFILE)3/$(T_OUT)3.png

clean_sample:
	rm $(TFILE)1/*.png $(TFILE)2/*.png $(TFILE)3/*.png
	rm $(TFILE)1/*.dot $(TFILE)2/*.dot $(TFILE)3/*.dot
	rm $(TFILE)1/$(T_OUT)1.res $(TFILE)2/$(T_OUT)2.res $(TFILE)3/$(T_OUT)3.res

clean_lsim:
	rm lsim

#--------------do maps-------------

all: compile maps  maps_to_graph

maps: $(SFILE1)/$(S_IN1).blif $(SFILE1)/$(S_IN1).sti $(SFILE2)/$(S_IN2).blif $(SFILE2)/$(S_IN2).sti $(SFILE3)/$(S_IN3).blif $(SFILE3)/$(S_IN3).sti $(SFILE4)/$(S_IN4).blif $(SFILE4)/$(S_IN4).sti $(SFILE5)/$(S_IN5).blif $(SFILE5)/$(S_IN5).sti
	$(run) $(SFILE1)/$(S_IN1).blif $(SFILE1)/$(S_IN1).sti $(SFILE1)/$(S_IN1).res
	mv $(OUT_DOT).dot $(SFILE1)/$(OUT_DOT).dot
	$(run) $(SFILE2)/$(S_IN2).blif $(SFILE2)/$(S_IN2).sti $(SFILE2)/$(S_IN2).res
	mv $(OUT_DOT).dot $(SFILE2)/$(OUT_DOT).dot
	$(run) $(SFILE3)/$(S_IN3).blif $(SFILE3)/$(S_IN3).sti $(SFILE3)/$(S_IN3).res
	mv $(OUT_DOT).dot $(SFILE3)/$(OUT_DOT).dot
	$(run) $(SFILE4)/$(S_IN4).blif $(SFILE4)/$(S_IN4).sti $(SFILE4)/$(S_IN4).res
	mv $(OUT_DOT).dot $(SFILE4)/$(OUT_DOT).dot
	$(run) $(SFILE5)/$(S_IN5).blif $(SFILE5)/$(S_IN5).sti $(SFILE5)/$(S_IN5).res
	mv $(OUT_DOT).dot $(SFILE5)/$(OUT_DOT).dot

maps_to_graph:	$(SFILE1)/$(OUT_DOT).dot $(SFILE2)/$(OUT_DOT).dot $(SFILE3)/$(OUT_DOT).dot $(SFILE4)/$(OUT_DOT).dot $(SFILE5)/$(OUT_DOT).dot
	$(DOT_png) $(SFILE1)/$(OUT_DOT).dot > $(SFILE1)/$(S_OUT1).png
	$(DOT_png) $(SFILE2)/$(OUT_DOT).dot > $(SFILE2)/$(S_OUT2).png
	$(DOT_png) $(SFILE3)/$(OUT_DOT).dot > $(SFILE3)/$(S_OUT3).png
	$(DOT_png) $(SFILE4)/$(OUT_DOT).dot > $(SFILE4)/$(S_OUT4).png
	$(DOT_png) $(SFILE5)/$(OUT_DOT).dot > $(SFILE5)/$(S_OUT5).png

clean_all:
	rm $(SFILE1)/*.png $(SFILE2)/*.png $(SFILE3)/*.png $(SFILE4)/*.png $(SFILE5)/*.png
	rm $(SFILE1)/*.dot $(SFILE2)/*.dot $(SFILE3)/*.dot $(SFILE4)/*.dot $(SFILE5)/*.dot
	rm $(SFILE1)/$(S_OUT1).res $(SFILE2)/$(S_OUT2).res $(SFILE3)/$(S_OUT3).res $(SFILE4)/$(S_OUT4).res $(SFILE5)/$(S_OUT5).res
