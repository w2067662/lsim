#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include <stack>
#include <queue>
#include <set>

using namespace std;
//typedef bool (*gate_func_one)(bool);
//typedef bool (*gate_func_two)(bool, bool);
//typedef bool (*gate_func_three)(bool, bool, bool);

enum gateType {
	INPUT,
	OUTPUT,
	zero,
	one,
	NOT,
	NAND2,
	NOR2,
	BUF,
	NAND3,
	NOR3,
	AND2,
	OR2,
	AND3,
	OR3,
	XOR2,
	XNOR2,
};

class Gate {
private:
	gateType type;
	int area;
public:
	Gate() : type(zero){
		area = 0;
	}
	Gate(gateType type) {
		this->type = type;
		switch (type) {
		case zero:case one:
			area = 0;
			break;
		case NOT:
			area = 2;
			break;
		case NAND2:case NOR2:
			area = 3;
			break;
		case BUF:case NAND3:case NOR3:case AND2:case OR2:
			area = 4;
			break;
		case AND3:
			area = 5;
			break;
		case OR3:
			area = 6;
			break;
		case XOR2:case XNOR2:
			area = 3;
			break;
		}
	}

	void setType(gateType type) {
		this->type = type;
		switch (type) {
		case zero:case one:
			area = 0;
			break;
		case NOT:
			area = 2;
			break;
		case NAND2:case NOR2:
			area = 3;
			break;
		case BUF:case NAND3:case NOR3:case AND2:case OR2:
			area = 4;
			break;
		case AND3:
			area = 5;
			break;
		case OR3:
			area = 6;
			break;
		case XOR2:case XNOR2:
			area = 3;
			break;
		}
	}
	gateType getType(void) { return type; }

	bool zero_Gate(bool a) { return 0; }
	bool one_Gate(bool a) { return 1; }
	bool NOT_Gate(bool a) { return !a; }
	bool BUF_Gate(bool a) { return a; }

	bool NAND2_Gate(bool a, bool b) { return !(a && b); }
	bool NOR2_Gate(bool a, bool b) { return !(a || b); }
	bool AND2_Gate(bool a, bool b) { return (a && b); }
	bool OR2_Gate(bool a, bool b) { return (a || b); }
	bool XOR2_Gate(bool a, bool b) { return ((!a && b) || (a && !b)); }
	bool XNOR2_Gate(bool a, bool b) { return ((a && b) || (!a && !b)); }

	bool NAND3_Gate(bool a, bool b, bool c) { return !(a && b && c); }
	bool NOR3_Gate(bool a, bool b, bool c) { return !(a || b || c); }
	bool AND3_Gate(bool a, bool b, bool c) { return (a && b && c); }
	bool OR3_Gate(bool a, bool b, bool c) { return (a || b || c); }

	friend ostream& operator<< (ostream& ostrm, const Gate& gate) {
		ostrm << "gate type: " << gate.type;
		ostrm << " area: " << gate.area << endl;
		return ostrm;
	}
};

struct node {
private:
	bool bit;
	bool settled;
	int level;
	string name;
	vector<node*> In;
	vector<node*> Out;
	Gate gate;

public:
	node(void) :name(""), level(0), bit(false), settled(false){}

	node(string gateType) :name(""), level(0), bit(false), settled(false) {
		if (gateType == "INPUT") {
			gate.setType(INPUT); settled = true;
		}
		else if (gateType == "OUTPUT")  gate.setType(OUTPUT);
		else if (gateType == "zero")  gate.setType(zero);
		else if (gateType == "one")  gate.setType(one);
		else if (gateType == "NOT")  gate.setType(NOT);
		else if (gateType == "NAND2")  gate.setType(NAND2);
		else if (gateType == "NOR2")  gate.setType(NOR2);
		else if (gateType == "BUF")  gate.setType(BUF);
		else if (gateType == "NAND3")  gate.setType(NAND3);
		else if (gateType == "NOR3")  gate.setType(NOR3);
		else if (gateType == "AND2")  gate.setType(AND2);
		else if (gateType == "OR2")  gate.setType(OR2);
		else if (gateType == "AND3")  gate.setType(AND3);
		else if (gateType == "OR3")  gate.setType(OR3);
		else if (gateType == "XOR2")  gate.setType(XOR2);
		else if (gateType == "XNOR2")  gate.setType(XNOR2);
	}

	node(string gateType, string name) :name(name), level(0), bit(false), settled(false) {
		if (gateType == "INPUT") {
			gate.setType(INPUT); settled = true;
		}
		else if (gateType == "OUTPUT")  gate.setType(OUTPUT);
		else if (gateType == "zero")  gate.setType(zero);
		else if (gateType == "one")  gate.setType(one);
		else if (gateType == "NOT")  gate.setType(NOT);
		else if (gateType == "NAND2")  gate.setType(NAND2);
		else if (gateType == "NOR2")  gate.setType(NOR2);
		else if (gateType == "BUF")  gate.setType(BUF);
		else if (gateType == "NAND3")  gate.setType(NAND3);
		else if (gateType == "NOR3")  gate.setType(NOR3);
		else if (gateType == "AND2")  gate.setType(AND2);
		else if (gateType == "OR2")  gate.setType(OR2);
		else if (gateType == "AND3")  gate.setType(AND3);
		else if (gateType == "OR3")  gate.setType(OR3);
		else if (gateType == "XOR2")  gate.setType(XOR2);
		else if (gateType == "XNOR2")  gate.setType(XNOR2);
	}

	void push_back_InNode(node* inNode) {
		In.push_back(inNode);
	}

	void push_back_OutNode(node* outNode) {
		Out.push_back(outNode);
	}

	vector<node*>& InNode(void) { return In; }
	vector<node*>& OutNode(void) { return Out; }

	bool isSettled(void) { return settled; }
	void SettledUp(bool value) { settled = value; }

	//bool isUsed()

	void runGate(void) {
		/*bool NotSettled = false;
		for (int i = 0; i < In.size(); i++) {
			if(!In[i]->isSettled())NotSettled = true;
		}*/

		//if (NotSettled) cout << name << "'s inNode is not settled"<< endl;
		//else {
			switch (gate.getType()) {
			case zero: setBit(gate.zero_Gate(In[0]->getBit())); break;
			case one:  setBit(gate.one_Gate(In[0]->getBit())); break;
			case NOT:  setBit(gate.NOT_Gate(In[0]->getBit())); break;
			case BUF:  setBit(gate.BUF_Gate(In[0]->getBit())); break;
			case NAND2:setBit(gate.NAND2_Gate(In[0]->getBit(), In[1]->getBit())); break;
			case NOR2: setBit(gate.NOR2_Gate(In[0]->getBit(), In[1]->getBit())); break;
			case AND2: setBit(gate.AND2_Gate(In[0]->getBit(), In[1]->getBit())); break;
			case OR2:  setBit(gate.OR2_Gate(In[0]->getBit(), In[1]->getBit())); break;
			case XOR2: setBit(gate.XOR2_Gate(In[0]->getBit(), In[1]->getBit())); break;
			case XNOR2:setBit(gate.XNOR2_Gate(In[0]->getBit(), In[1]->getBit())); break;
			case NAND3:setBit(gate.NAND3_Gate(In[0]->getBit(), In[1]->getBit(), In[2]->getBit())); break;
			case NOR3: setBit(gate.NOR3_Gate(In[0]->getBit(), In[1]->getBit(), In[2]->getBit())); break;
			case AND3: setBit(gate.AND3_Gate(In[0]->getBit(), In[1]->getBit(), In[2]->getBit())); break;
			case OR3:  setBit(gate.OR3_Gate(In[0]->getBit(), In[1]->getBit(), In[2]->getBit())); break;
			}
			//cout << "gate type" << getGateType();
			//cout <<"node name" << name <<" output bit ="<< bit << endl;
			settled = true;
		//}
	}

	bool getBit(void) { return bit; }
	void setBit(bool bit) { this->bit = bit; }

	void setLevel(int level) { this->level = level; }
	int getLevel(void) { return level; }
	void updateLevel(void) {
		int Max = 0;
		for (int i = 0; i < In.size(); i++) {
			if (In[i]->level > Max)Max = In[i]->level;
		}
		this->level = Max + 1;
	}

	string getNodeName(void) { return name; }
	void setNodeName(string name) { this->name = name; }

	gateType getGateType(void) { return gate.getType(); }
	void setGateType(gateType  gateType) { gate.setType(gateType); }
	void setGateType(string gateType) {
		if (gateType == "INPUT") gate.setType(INPUT);
		else if (gateType == "OUTPUT")  gate.setType(OUTPUT);
		else if (gateType == "zero")  gate.setType(zero);
		else if (gateType == "one")  gate.setType(one);
		else if (gateType == "NOT")  gate.setType(NOT);
		else if (gateType == "NAND2")  gate.setType(NAND2);
		else if (gateType == "NOR2")  gate.setType(NOR2);
		else if (gateType == "BUF")  gate.setType(BUF);
		else if (gateType == "NAND3")  gate.setType(NAND3);
		else if (gateType == "NOR3")  gate.setType(NOR3);
		else if (gateType == "AND2")  gate.setType(AND2);
		else if (gateType == "OR2")  gate.setType(OR2);
		else if (gateType == "AND3")  gate.setType(AND3);
		else if (gateType == "OR3")  gate.setType(OR3);
		else if (gateType == "XOR2")  gate.setType(XOR2);
		else if (gateType == "XNOR2")  gate.setType(XNOR2);
	}

	friend ostream& operator<< (ostream& ostrm, const node& node) {
		ostrm << "node name: " << node.name;
		ostrm << " level: " << node.level;
		ostrm << " bit:" << node.bit;
		ostrm << " " << node.gate;
		ostrm << "In: ";
		for (int i = 0; i < node.In.size(); i++) {
			ostrm << node.In[i]->name << "-> ";
		}ostrm << endl;
		ostrm << "Out: ";
		for (int i = 0; i < node.Out.size(); i++) {
			ostrm << "->" << node.Out[i]->name;
		}ostrm << endl << endl;
		return ostrm;
	}
};

class circuitGraph {
private:
	vector<node*> inputNodes;
	vector<node*> outputNodes;
	map<int, vector<node*>> levelmap;
	vector<string> nodeIndex;
	unordered_map<string, node*> nodes;
	int inputSize;
	int outputSize;
	int GateAmount;
public:
	circuitGraph() :inputSize(0), outputSize(0), GateAmount(0) {}

	int size(void) {
		return nodes.size();
	}

	void insertEdge(string inNode, string outNode) {
		node* from = nodes[inNode];
		node* to = nodes[outNode];
		from->push_back_OutNode(to);
		to->push_back_InNode(from);
	}

	void setIndex(string name) { this->nodeIndex.push_back(name); }

	bool insertNode(string name, node* node) {
		if (nodes.find(name) == nodes.end()) {//not find
			nodes.insert({ name, node });
			return true;
		}
		else {
			nodes[name]->setGateType(node->getGateType());
			return false;
		}
		
	}

	void push_back_inputNode(node* node) {
		inputNodes.push_back(node);
	}
	void push_back_outputNode(node* node) {
		outputNodes.push_back(node);
	}

	bool findNode(string name) { return (nodes.find(name) != nodes.end()); }
	node* getNode(string name) { return nodes[name]; }

	vector<vector<bool>> LogicSimulation(const vector<vector<bool>>& inBits) {
		vector<vector<bool>> outBits;

		for (int j = 0; j < inBits.size(); j++) {
			for (int i = 0; i < inputNodes.size(); i++) {
				inputNodes[i]->setBit(inBits[j][i]);
				cout << inputNodes[i]->getBit();
			}cout << " ";

			for (const auto& element : levelmap) {
				for (int i = 0; i < element.second.size(); i++) {
					element.second[i]->runGate();
					//if(element.second[i]->getGateType()==OUTPUT)cout << element.second[i]->getBit() << endl;
				}
			}

			vector<bool> temp;
			for (int i = 0; i < outputNodes.size(); i++) {
				temp.push_back(outputNodes[i]->getBit());
				cout << outputNodes[i]->getBit();
			}
			outBits.push_back(temp);
			cout << endl;
		}

		return outBits;
	}

	//vector<vector<bool>> LogicSimulationDFS (const vector<vector<bool>>& inBits) {
	//	vector<vector<bool>> outBits;
	//	set<string> used;
	//	queue<node*> nextNodes;

	//	for (int j = 0; j < inBits.size(); j++) {
	//		for (int i = 0; i < inputNodes.size(); i++) {
	//			inputNodes[i]->setBit(inBits[j][i]);
	//			//cout << inputNodes[i]->getBit();
	//		}

	//		for (int i = 0; i < inputNodes.size();i++) {
	//			for (int i = 0; i < element.second.size(); i++) {
	//				element.second[i]->runGate();
	//				//if(element.second[i]->getGateType()==OUTPUT)cout << element.second[i]->getBit() << endl;
	//			}
	//		}

	//		vector<bool> temp;
	//		for (int i = 0; i < outputNodes.size(); i++) {
	//			//cout << outputNodes[i]->getBit();
	//			temp.push_back(outputNodes[i]->getBit());
	//		}
	//		outBits.push_back(temp);
	//	}

	//	return outBits;
	//}

	void levelize(void) {
		cout << "do logic simulation by level" << endl;
		for (int i = 0; i < nodeIndex.size(); i++) {
			if (nodes[nodeIndex[i]]->getLevel() != 0) {
				int level = nodes[nodeIndex[i]]->getLevel();
				if (levelmap.find(level) != levelmap.end()) {
					levelmap[level].push_back(nodes[nodeIndex[i]]);
				}
				else {
					vector<node*> temp;
					temp.push_back(nodes[nodeIndex[i]]);
					levelmap.insert({ nodes[nodeIndex[i]]->getLevel() , temp });
				}
			}
		}

		//printLevelList();
	}

	void printByIndex(void) {
		cout << "print by index" << endl;
		for (int i = 0; i < nodeIndex.size(); i++) {
			cout << nodeIndex[i] << endl;
		}
	}

	void findCycle(void) {
		set<node*>used;
		queue<node*> nextNode;

		cout << endl << "finding cycle..." << endl;
		for (int i = 0; i < inputNodes.size(); i++) {
			nextNode.push(inputNodes[i]);
			cout << endl << "start node:" << inputNodes[i]->getNodeName() << "(" << inputNodes[i] << ")" << endl;

			while (!nextNode.empty()) {
				node* curr = nextNode.front();
				nextNode.pop();

				cout << " -> " << curr->getNodeName() << "(" << curr << ")";

				for (int j = 0; j < nodes[curr->getNodeName()]->OutNode().size(); j++) {
					node* next = nodes[curr->getNodeName()]->OutNode()[j];
					if (used.find(next) != used.end()) {
						cout << endl << "there is cycle in " << nodes[curr->getNodeName()]->OutNode()[j]->getNodeName() << "(" << inputNodes[i] << ")" << endl;
					}
					else {
						used.insert(next);
						nextNode.push(next);
					}
				}
			}

			used.clear();
		}
		cout << endl;
	}

	void print(void) {
		for (const auto& node : nodes) {
			cout << *node.second;
		}
	}

	void printLevelList(void) {
		cout << "head-> ";
		for (const auto& element : levelmap) {
			for (int i = 0; i < element.second.size(); i++) {
				cout << element.second[i]->getNodeName() << "(" << element.second[i]->getLevel() << ")" << " -> ";
			}
		}cout << "tail" << endl;
	}

	void printOutNodes(void) {
		for (int i = 0; i < outputNodes.size(); i++) {
			cout << outputNodes[i]->getNodeName();
			cout << "("<<outputNodes[i] <<")"<< endl;
		}
	}

	void outputDOT(ofstream& outfile_dot) {
		//digraph obdd(robdd){
		outfile_dot << "digraph  LOGIC_GATE  {" << endl;
		//rankdir = LR;
		outfile_dot << "\trankdir = LR;" << endl << endl;
		//{rank=same sequence_number}
		outfile_dot << "\t" << "{rank=same";
		for (int i = 0; i < inputNodes.size(); i++) {
			outfile_dot << " " << inputNodes[i]->getNodeName();
		}outfile_dot << "}" << endl;

		for (const auto& element : levelmap) {
			bool check = false;
			for (int i = 0; i < element.second.size(); i++) {
				if (!check) {
					outfile_dot << "\t" << "{rank=same " << element.second[i]->getNodeName();
					check = true;
				}
				else outfile_dot << " " << element.second[i]->getNodeName();
			}
			if (check) outfile_dot << "};\n";
		}

		outfile_dot << endl;
		//inputNodes [label="inputNodes", shape=box];
//		for (int i = 0; i < inputNodes.size(); i++) {
//			outfile_dot << "\t" << inputNodes[i]->getNodeName() << "[label=\"" << inputNodes[i]->getNodeName() << "\", shape=box];" << endl;
//		}
		//sequence_nubmer[label = var];
		for (int i = 0; i < nodeIndex.size(); i++) {
			outfile_dot << "\t" << nodes[nodeIndex[i]]->getNodeName() << " [label=\"" << nodes[nodeIndex[i]]->getNodeName() << "\"];\n";
		}
		//outputNodes [label="outputNodes", shape=box];
		for (int i = 0; i < outputNodes.size(); i++) {
			outfile_dot << "\t" << outputNodes[i]->getNodeName() << "[label=\"" << outputNodes[i]->getNodeName() << "\", shape=box];" << endl;
		}

		outfile_dot << endl;
		for (int i = 0; i < nodeIndex.size(); i++) {
			for (int j = 0; j < nodes[nodeIndex[i]]->OutNode().size(); j++) {
				outfile_dot << "\t" << nodes[nodeIndex[i]]->getNodeName() << " -> " << nodes[nodeIndex[i]]->OutNode()[j]->getNodeName() << "[style = solid];" << endl;
			}
		}
		//}
		outfile_dot << "}";
		outfile_dot.close();
	}
};

int main(int argc, char** argv) {
	//if (1) {
	//	ifstream infile_blif("test3.blif");
	//	ifstream infile_sti("test3.sti");
	//	ofstream outfile_res("test3.res");
	//	ofstream outfile_dot("out.dot");

		if (argc == 4) {
			ifstream infile_blif(argv[1]);
			ifstream infile_sti(argv[2]);
			ofstream outfile_res(argv[3]);
			ofstream outfile_dot("out.dot"); 

		//variable for BLIF file
		int inSize = 0;
		int outSize = 0;
		bool start_flag = false;
		string modelName;
		circuitGraph graph;

		//variable for STI file
		int inputAmount = 0;
		vector<vector<bool>> inputBits;

		//variable for RES file
		vector<vector<bool>> outputBits;

		string line;
		while (infile_blif >> line) {  //read blif
			if (line == ".model") {
				start_flag = true;

				infile_blif >> modelName;
				continue;
			}
			else if (line == ".inputs") {
				string input;
				string inputNodeName = "";

				nextline_in:
				getline(infile_blif, input);
				//cout << input << endl;

				for (int i = 0; i < input.size(); i++) {
					if (input[i] == ' ') {
						if (inputNodeName.size() != 0) {
							inSize++;
							node* new_node = new node("INPUT", inputNodeName);
							if (graph.insertNode(inputNodeName, new_node)) graph.setIndex(inputNodeName);
							graph.push_back_inputNode(new_node);
							inputNodeName = "";
						}
					}
					else if (i == input.size() - 1) {
						if(input[i] == '\\'){
							goto nextline_in;
						}
						else {
							inputNodeName += input[i];

							inSize++;
							node* new_node = new node("INPUT", inputNodeName);
							if (graph.insertNode(inputNodeName, new_node)) graph.setIndex(inputNodeName);
							graph.push_back_inputNode(new_node);
							inputNodeName = "";
							break;
						}
					}
					else  inputNodeName += input[i];
				}

				//graph.print();

			}
			else if (line == ".outputs") {
				string input;
				string outputNodeName = "";

				nextline_out:
				getline(infile_blif, input);

				for (int i = 0; i < input.size(); i++) {
					if (input[i] == ' ') {
						if (outputNodeName.size() != 0) {
							outSize++;
							node* new_node = new node("OUTPUT", outputNodeName);
							if (graph.insertNode(outputNodeName, new_node))	graph.setIndex(outputNodeName);
							graph.push_back_outputNode(new_node);
							outputNodeName = "";
						}
					}
					else if (i == input.size() - 1) {
						if (input[i] == '\\') {
							goto nextline_out;
						}
						else {
							outputNodeName += input[i];

							outSize++;
							node* new_node = new node("OUTPUT", outputNodeName);
							if (graph.insertNode(outputNodeName, new_node))	graph.setIndex(outputNodeName);
							graph.push_back_outputNode(new_node);
							outputNodeName = "";
							break;
						}
					}
					else  outputNodeName += input[i];
				}

				//graph.print();
			}
			else if (line == ".gate") {
				string type;
				infile_blif >> type;
				node* new_node = new node(type);

				string a = "", b = "", c = "", O = "";

				switch (new_node->getGateType()) {
				case zero:case one:case NOT:case BUF:
					infile_blif >> a >> O;
					a.erase(0, 2);
					O.erase(0, 2);

					new_node->setNodeName(O);
					if (graph.insertNode(O, new_node)) graph.setIndex(O);

					graph.insertEdge(a, O);

					graph.getNode(O)->updateLevel();

					break;
				case NAND2:case NOR2:case AND2:case OR2:case XOR2:case XNOR2:
					infile_blif >> a >> b >> O;
					a.erase(0, 2);
					b.erase(0, 2);
					O.erase(0, 2);

					new_node->setNodeName(O);
					if (graph.insertNode(O, new_node)) graph.setIndex(O);

					graph.insertEdge(a, O);
					graph.insertEdge(b, O);

					graph.getNode(O)->updateLevel();

					break;
				case NAND3:case NOR3:case AND3:case OR3:
					infile_blif >> a >> b >> c >> O;
					a.erase(0, 2);
					b.erase(0, 2);
					c.erase(0, 2);
					O.erase(0, 2);

					new_node->setNodeName(O);
					if (graph.insertNode(O, new_node)) graph.setIndex(O);

					graph.insertEdge(a, O);
					graph.insertEdge(b, O);
					graph.insertEdge(c, O);

					graph.getNode(O)->updateLevel();

					break;
				}
			}
			else if (line == ".end") {
				start_flag = false;

				//graph.print();
				graph.levelize();
				//graph.printByIndex();
				//graph.findCycle();
				break;
			}
		}
		infile_blif.close();


		//read input.sti
		while (infile_sti >> line) {
			if (line == ".i") {
				infile_sti >> inputAmount;
			}
			else if (line == ".e") {
				break;
			}
			else {
				vector<bool> temp;
				for (int i = 0; i < line.size(); i++) {
					if (line[i] == '1') temp.push_back(1);
					else if (line[i] == '0') temp.push_back(0);
				}
				inputBits.push_back(temp);
			}
		}
		infile_sti.close();

		outputBits = graph.LogicSimulation(inputBits);
		//graph.print();
		//graph.printOutNodes();

		//output.res
		outfile_res << ".i " << inSize << endl;
		outfile_res << ".o " << outSize << endl;
		for (int j = 0; j < inputBits.size(); j++) {
			for (int i = 0; i < inputBits[j].size(); i++) {
				outfile_res << inputBits[j][i];
			}
			outfile_res << " ";
			for (int i = 0; i < outputBits[j].size(); i++) {
				outfile_res << outputBits[j][i];
			}
			outfile_res << endl;
		}
		outfile_res << ".e" << endl;

		outfile_res << "#result of model " << modelName << " logic simulation" << endl;
		outfile_res.close();
		//cout << argv[3] << " ...file output complete" << endl;


		graph.outputDOT(outfile_dot);


		cout << "model " << modelName << " logic simulation ...complete" << endl;

	}
	else {
		cout << "wrong format!Please use:" << endl;
		cout << ">$ lsim simple.blif simple.sti simple.res" << endl;
	}
}
