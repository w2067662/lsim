#you can simply use makefile commands to do all the instructions
Makefile commands:
>$ make all     # read input.blif and input.sti and do logic simulation,
		# then output output.res (out.dot)
>$ make sample   # do logic simulation to sample cases


>$ make clean_all   # remove test output .DOT .PNG .RES files
>$ make clean_sample   # remove sample output .DOT .PNG .RES files


#Explanations
use the following command to create "lsim" file:
>$ g++ source.cpp -o lsim -std=c++11 -lm


execute "lsim" to read .BLIF .STI file and output .RES file:
>$ ./lsim input.blif input.sti output.res
